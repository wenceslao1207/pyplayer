#! /bin/bash

cp ./src/pyplayer ~/.local/bin
chmod +x ~/.local/bin/pyplayer
cp ./src/spotify_notification ~/.local/bin
chmod +x ~/.local/bin/spotify_notification
